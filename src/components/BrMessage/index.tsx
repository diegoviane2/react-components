import { MessageIcon, MessageType } from "@/types/messages";
import classNames from "classnames";
import { ReactNode } from "react";
import BrButton from "../BrButton";

export interface BrMessageProps {
  id?: string;
  type: MessageType;
  message: ReactNode;
  title?: string;
  inlineTitle?: boolean;
  onClose?: () => void;
}

const BrMessage = (props: BrMessageProps) => {
  const { id, message, type, title, inlineTitle, onClose } = props;

  const classes = classNames("br-message", type);

  const BodyWrapper = inlineTitle ? "span" : "p";

  return (
    <div className={classes} id={id} role="alert">
      <div className="icon">
        <i className={`fas fa-${MessageIcon[type]} fa-lg`} aria-hidden="true"></i>
      </div>
      <div className="content">
        {title && <span className="message-title mr-2">{title}</span>}
        <BodyWrapper className="message-body">{message}</BodyWrapper>
      </div>
      {onClose && (
        <div className="close">
          <BrButton circle size="small" aria-label="Fechar" onClick={onClose}>
            <i className="fas fa-times" aria-hidden="true"></i>
          </BrButton>
        </div>
      )}
    </div>
  );
};

export default BrMessage;
