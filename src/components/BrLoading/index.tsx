import Container from "@/common/Container";
import { Col, Row } from "@/common/Grid";

export type BrLoadingProps = {
  label?: string;
  medium?: boolean;
};

const BrLoading = ({ label, medium }: BrLoadingProps) => {
  const classes = medium ? "loading medium" : "loading";

  return (
    <Container>
      <Row column alignItems="center">
        <Col>
          <div className={classes}></div>
        </Col>
        <Col margin={{ t: 2 }}>{label && <span className="rotulo">{label}</span>}</Col>
      </Row>
    </Container>
  );
};

export default BrLoading;
