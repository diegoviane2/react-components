import { Story, Meta } from "@storybook/react";

import { Col } from "@/common/Grid";

import BrDateTimePicker, { BrDateTimePickerProps } from ".";

export default {
  title: "Input/BrDateTimePicker",
  component: BrDateTimePicker,
} as Meta;

const Template: Story<BrDateTimePickerProps> = (args) => {
  return (
    <Col sm={6} md={4} lg={3}>
      <BrDateTimePicker {...args} onClose={() => ""} />
    </Col>
  );
};

export const DatePicker = Template.bind({});

export const TimePicker = Template.bind({});
TimePicker.args = { ...TimePicker.args, type: "time" };

export const TimePickerSegundos = Template.bind({});
TimePickerSegundos.args = {
  ...TimePickerSegundos.args,
  type: "time",
  timeProps: { enableSeconds: true },
};

export const DateTimePicker = Template.bind({});
DateTimePicker.args = { ...DateTimePicker.args, type: "dateTime" };
