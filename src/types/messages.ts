export type MessageType = "success" | "info" | "warning" | "danger";

export const MessageIcon: Record<MessageType, string> = {
  success: "check-circle",
  info: "info-circle",
  warning: "exclamation-triangle",
  danger: "times-circle",
};
